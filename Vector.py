import math


class Vector:
    def __init__(self, x, y, z=0):
        self.x = x
        self.y = y
        self.z = z

    @staticmethod
    def fromVector(v):
        return Vector(v.x, v.y, x.z)

    def __str__(self):
        return f'[{self.x}, {self.y}, {self.z}]'

    def copy(self):
        return Vector(self.x, self.y, self.z)

    def __add__(self, x, y=0, z=0):
        if type(x) is Vector:
            return Vector(self.x + x.x,
                          self.y + x.y,
                          self.y + x.z)
        else:
            return Vector(self.x + x,
                          self.y + y,
                          self.y + z)

    def __sub__(self, x, y=0, z=0):
        if type(x) is Vector:
            return Vector(self.x - x.x,
                          self.y - x.y,
                          self.y - x.z)
        else:
            return Vector(self.x - x,
                          self.y - y,
                          self.y - z)

    def __mul__(self, x, y=0, z=0):
        if type(x) is Vector:
            return Vector(self.x * x.x,
                          self.y * x.y,
                          self.y * x.z)
        else:
            return Vector(self.x * x,
                          self.y * y,
                          self.y * z)

    def __div__(self, x, y=0, z=0):
        if type(x) is Vector:
            return Vector(self.x / x.x,
                          self.y / x.y,
                          self.y / x.z)
        else:
            return Vector(self.x / x,
                          self.y / y,
                          self.y / z)

    def __floordiv__(self, x, y=0, z=0):
        if type(x) is Vector:
            return Vector(self.x // x.x,
                          self.y // x.y,
                          self.y // x.z)
        else:
            return Vector(self.x // x,
                          self.y // y,
                          self.y // z)

    def __inv__(self):
        return Vector(-self.x,
                      -self.y,
                      -self.y)

    def __mod__(self, x, y=0, z=0):
        if type(x) is Vector:
            return Vector(self.x % x.x,
                          self.y % x.y,
                          self.y % x.z)
        else:
            return Vector(self.x % x,
                          self.y % y,
                          self.y % z)

    def __pow__(self, x, y=0, z=0):
        if type(x) is Vector:
            return Vector(self.x ** x.x,
                          self.y ** x.y,
                          self.y ** x.z)
        else:
            return Vector(self.x ** x,
                          self.y ** y,
                          self.y ** z)

    def calculateRemainder2D(self, xComponent, yComponent):
        if xComponent:
            self.x = self.x % xComponent
        if yComponent:
            self.y = self.y % yComponent
        return self

    def calculateRemainder2D(self, xComponent, yComponent, zComponent):
        if xComponent:
            self.x = self.x % xComponent
        if yComponent:
            self.y = self.y % yComponent
        if zComponent:
            self.y = self.y % zComponent
        return self

    def mag(self):
        return math.sqrt(self.magSq())

    def magSq(self):
        return self.x * self.x + self.y * self.y + self.z * self.z

    def dot(self, x, y=0, z=0):
        if type(x) is Vector:
            return self.dot(x.x, x.y, x.z)
        return self.x * (x | 0) + self.y * (y | 0) + self.z * (z | 0)

    def cross(self, v):
        new_x = self.y * v.z - self.z * v.y
        new_y = self.z * v.x - self.x * v.z
        new_z = self.x * v.y - self.y * v.x
        return Vector(new_x, new_y, new_z)

    def dist(self, v):
        return (v.copy() - self).mag()

    def normalize(self):
        length = self.mag()
        if length != 0:
            self * (1 / length)
        return self

    def limit(self, maximum):
        mSq = self.magSq()
        if (mSq > maximum * maximum):
            self / math.sqrt(mSq) * maximum
        return self

    def setMag(self, n):
        self.normalize()
        self *= n
        return self

    def heading(self):
        h = math.atan2(self.y, self.x)
        return h

    def rotate(self, a):
        newHeading = self.heading() + a
        mag = self.mag()
        self.x = math.cos(newHeading) * mag
        self.y = math.sin(newHeading) * mag
        return self

    def angleBetween(self, v):
        dotmagmag = self.dot(v) / (self.mag() * v.mag())
        angle = math.acos(math.min(1, math.max(-1, dotmagmag)))
        angle = angle * math.sign(self.cross(v).z | 1)
        return angle

    def lerp(self, x, y, z, amt):
        if type(x) is Vector:
            return self.lerp(x.x, x.y, x.z, y)
        self.x += (x - self.x) * amt | 0
        self.y += (y - self.y) * amt | 0
        self.z += (z - self.z) * amt | 0
        return self

    def reflect(self, surfaceNormal):
        surfaceNormal.normalize()
        return self - (surfaceNormal * (2 * self.dot(surfaceNormal)))

    def array(self):
        return [self.x, self.y, self.z]

    def __eq__(self, v):
        return self.x == v.x and self.y == v.y and self.z == v.z

    @staticmethod
    def fromAngle(angle, length=1):
        return Vector(length * math.cos(angle), length * math.sin(angle))

    @staticmethod
    def fromAngles(theta, phi, length=1):
        cosPhi = math.cos(phi)
        sinPhi = math.sin(phi)
        cosTheta = math.cos(theta)
        sinTheta = math.sin(theta)

        return Vector(length * sinTheta * sinPhi, -length * cosTheta, length * sinTheta * cosPhi)

