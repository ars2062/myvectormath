from setuptools import setup, find_packages

with open('README.md') as readme_file:
    README = readme_file.read()

with open('HISTORY.md') as history_file:
    HISTORY = history_file.read()

setup_args = dict(
    name='myvectormath',
    version='0.0.1',
    description='Useful Vector maths for Python',
    long_description_content_type="text/markdown",
    long_description=README + '\n\n' + HISTORY,
    license='MIT',
    packages=find_packages(),
    author='Arshia Moghaddam',
    author_email='ars2062@gmail.com',
    keywords=['vector','math'],
    url='https://gitlab.com/ars2062/myvectormath',
    download_url='https://pypi.org/project/myvectormath/'
)

install_requires = [
]

if __name__ == '__main__':
    setup(**setup_args, install_requires=install_requires)
